using System;

namespace WebsiteShared.Model
{
    public class WeatherForecast
    {
        public DateTime Date { get; set; }

        public int TemperatureC { get; set; }

        public int TemperatureF => this.CelsiusToFahrenheit(TemperatureC);

        public string Summary { get; set; }

        public int CelsiusToFahrenheit(int TemperatureC)
        {
            return (int)Math.Round(32 + (TemperatureC / (5.0 / 9.0)), MidpointRounding.AwayFromZero);
        }

    }

}
