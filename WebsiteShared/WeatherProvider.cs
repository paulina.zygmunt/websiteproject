﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using WebsiteShared.Model;

namespace WebsiteShared
{
    public class WeatherProvider
    {
        public WeatherForecast[] GetWeatherForSzczecin()
        {
            using (WebClient webClient = new WebClient())
            {
                webClient.QueryString.Add("lat", "53.44");
                webClient.QueryString.Add("lon", "14.53");
                webClient.QueryString.Add("exclude", "hourly");
                webClient.QueryString.Add("appid", "eb63e8f92faa58a38103ef9d1a20d9d2");
                webClient.QueryString.Add("units", "metric");


                var json = webClient.DownloadString("https://api.openweathermap.org/data/2.5/onecall");
                var weather = JsonConvert.DeserializeObject<OpenWeatherMap>(json);
                var weatherForecasts = new List<WeatherForecast>();

                foreach (var dailyitem in weather.daily)
                {
                    var wf = new WeatherForecast
                    {
                        Date = UnixTimeStampToDateTime(dailyitem.dt),
                        TemperatureC = (int)Math.Round(dailyitem.temp.day, MidpointRounding.AwayFromZero),
                        Summary = dailyitem.weather[0].main
                    };
                    weatherForecasts.Add(wf);
                }

                return weatherForecasts.ToArray();
            }

        }
        private DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            var dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            return dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();

        }
    }
}
