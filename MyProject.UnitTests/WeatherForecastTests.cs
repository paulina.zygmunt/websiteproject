﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WebsiteShared.Model;

namespace MyProject.Model.Tests
{
    [TestClass]
    public class WeatherForecastTests
    {
        [TestMethod]
        public void CelsiusToFahrenheit_40_104()   // TestedMethodName_Input(Celsius)_ExpectedOutput(Fahrenheit)
        {
            var weather = new WeatherForecast();            // Arrange
            int result = weather.CelsiusToFahrenheit(40);   // Act
            Assert.AreEqual(104, result);                    // Assert
        }

        [TestMethod]
        public void CelsiusToFahrenheit_19_66()
        {
            var weather = new WeatherForecast();
            int result = weather.CelsiusToFahrenheit(19);
            Assert.AreEqual(66, result);
        }

        [TestMethod]
        public void CelsiusToFahrenheit_00_32()
        {
            var weather = new WeatherForecast();
            int result = weather.CelsiusToFahrenheit(0);
            Assert.AreEqual(32, result);
        }

        [TestMethod]
        public void CelsiusToFahrenheit_minus05_23()
        {
            var weather = new WeatherForecast();
            int result = weather.CelsiusToFahrenheit(-5);
            Assert.AreEqual(23, result);
        }

        [TestMethod]
        public void CelsiusToFahrenheit_minus08_18()
        {
            var weather = new WeatherForecast();
            int result = weather.CelsiusToFahrenheit(-8);
            Assert.AreEqual(18, result);
        }

        [TestMethod]
        public void CelsiusToFahrenheit_minus18_00()
        {
            var weather = new WeatherForecast();
            int result = weather.CelsiusToFahrenheit(-18);
            Assert.AreEqual(0, result);
        }

        [TestMethod]
        public void CelsiusToFahrenheit_minus20_minus04()
        {
            var weather = new WeatherForecast();
            int result = weather.CelsiusToFahrenheit(-20);
            Assert.AreEqual(-4, result);
        }


        [TestMethod]
        public void CelsiusToFahrenheit_01_34()
        {
            var weather = new WeatherForecast();
            int result = weather.CelsiusToFahrenheit(1);
            Assert.AreEqual(34, result);
        }

        [TestMethod]
        public void CelsiusToFahrenheit_02_36()
        {
            var weather = new WeatherForecast();
            int result = weather.CelsiusToFahrenheit(2);
            Assert.AreEqual(36, result);
        }

        [TestMethod]
        public void CelsiusToFahrenheit_03_37()
        {
            var weather = new WeatherForecast();
            int result = weather.CelsiusToFahrenheit(3);
            Assert.AreEqual(37, result);
        }

        [TestMethod]
        public void CelsiusToFahrenheit_04_39()
        {
            var weather = new WeatherForecast();
            int result = weather.CelsiusToFahrenheit(4);
            Assert.AreEqual(39, result);
        }

    }
}
